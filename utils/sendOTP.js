const msg91 = require("msg91")(process.env.MSG91_KEY, "GOGOTP", "4");
exports.to = async (phoneNo, name, OTP) => {
  const phoneNumber = phoneNo.toString();
  const message = `Hello ${name}, welcome to Gogo-Bikes your one time verification password is: ${OTP}`;
  await msg91.send(phoneNumber, message, (err, response) => {});
};
