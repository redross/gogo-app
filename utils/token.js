const jwt = require("jsonwebtoken");
exports.sign = user => {
  const token = jwt.sign(
    {
      iss: "Gogo-Bikes",
      id: user.id,
      name: user.name,
      phoneNumber: user.phoneNumber,
      iat: Math.floor(new Date().getTime() / 1000),
      exp: Math.floor(new Date().setDate(new Date().getDate() + 1) / 1000)
    },
    process.env.JWT_KEY
  );
  return token;
};
