const mongoose = require("mongoose");
const bikes = mongoose.model("bikes");

exports.check = async bikeId => {
  try {
    const bike = await bikes.findOne({ bikeId }).exec();
    return bike;
  } catch (error) {
    return error;
  }
};
