const mongoose = require("mongoose");
const users = mongoose.model("users");

exports.check = async phoneNumber => {
  try {
    const user = await users.findOne({ phoneNumber }).exec();
    return user;
  } catch (error) {
    return error;
  }
};

exports.id = async id => {
  try {
    const user = await users.findById(id).exec();
    return user;
  } catch (error) {
    return error;
  }
};
