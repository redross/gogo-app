exports.create = numberOfCodes => {
  let codes = [];
  for (let i = 0; i < numberOfCodes; i++) {
    const temp = Math.floor(1000 + Math.random() * 9000);
    codes[i] = temp;
  }
  return codes;
};
