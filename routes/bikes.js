const express = require("express");
const router = express.Router();
const passport = require("passport");

const jwtStrategy = require("../middlewares/passport");
const bike = require("../controllers/bikeController");
const admin = require("../middlewares/admin");
const money = require("../middlewares/money");
const bikeLocation = require("../middlewares/bikeLocation");

router.route("/admin/lockcode").post(admin.login, bike.postlockCodesAdmin);
router.route("/admin/bikedetails").post(admin.login, bike.detailsAdmin);
router
  .route("/rent")
  .post(
    passport.authenticate("jwt", { session: false }),
    bikeLocation.check,
    money.check,
    bike.getCode
  );

module.exports = router;
