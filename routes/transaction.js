const express = require("express");
const router = express.Router();
const passport = require("passport");

const jwtStrategy = require("../middlewares/passport");
const transaction = require("../controllers/transactionController");

router
  .route("/")
  .post(passport.authenticate("jwt", { session: false }), transaction.add)
  .get(passport.authenticate("jwt", { session: false }), transaction.get);

module.exports = router;
