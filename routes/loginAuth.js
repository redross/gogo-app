const express = require("express");
const router = express.Router();

const auth = require("../controllers/loginController");
const test = require("../middlewares/validation");

router.route("/").post(test.phoneNumber, auth.login);

module.exports = router;
