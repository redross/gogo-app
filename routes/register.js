const express = require("express");
const router = express.Router();

const form = require("../controllers/registerController");
const test = require("../middlewares/validation");

router.route("/").post(test.register, form.register);

module.exports = router;
