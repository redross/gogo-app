const express = require("express");
const router = express.Router();

const otp = require("../controllers/otpController");
const test = require("../middlewares/validation");

router.route("/sendOTP").post(test.phoneNumber, test.name, otp.sendOTP);
router.route("/checkotp").post(test.phoneNumber, test.otp, otp.checkOTP);

module.exports = router;
