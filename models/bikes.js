const mongoose = require("mongoose");
const schema = mongoose.Schema;

const bikesSchema = new schema(
  {
    bikeId: Number,
    lockCodes: [Number],
    status: Boolean,
    lastParkLocation: { type: String, default: null },
    lastUserId: { type: mongoose.Schema.Types.ObjectId, default: null }
  },
  { timestamps: true }
);
mongoose.model("bikes", bikesSchema);
