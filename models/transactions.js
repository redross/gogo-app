const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const transactionsSchema = new Schema(
  {
    userId: mongoose.Schema.Types.ObjectId,
    amount: Number,
    date: String
  },
  { timestamps: true }
);

mongoose.model("transactions", transactionsSchema);
