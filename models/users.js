const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const usersSchema = new Schema(
  {
    phoneNumber: Number,
    otp: Number,
    varifiedOtp: {
      type: Boolean,
      default: false
    },
    name: String,
    email: String,
    hostel: String,
    sex: String,
    password: String,
    money: Number,
    status: String,
    date: {
      type: Date,
      default: Date.now()
    }
  },
  { timestamps: true }
);

mongoose.model("users", usersSchema);
