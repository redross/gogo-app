exports.login = async (req, res, next) => {
  try {
    const { adminName, adminPassword } = req.body;
    if (
      adminName === process.env.ADMIN_NAME &&
      adminPassword === process.env.ADMIN_PASSWORD
    ) {
      return next();
    } else {
      return res.json({ msg: "Admin right access needed" });
    }
  } catch (error) {
    res.json({ error });
  }
};
