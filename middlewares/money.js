const monogoose = require("mongoose");
const users = monogoose.model("users");
const findUser = require("../utils/checkUser");

exports.check = async (req, res, next) => {
  try {
    const user = await findUser.id(req.user.id);
    if (user.money < 2) {
      return res.json({ msg: "you should atleast have 2 coins" });
    } else return next();
  } catch (error) {
    res.json({ error });
  }
};
