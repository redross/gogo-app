const passport = require("passport");
const JWT_strategy = require("passport-jwt").Strategy;
const { ExtractJwt } = require("passport-jwt");
const mongoose = require("mongoose");
const users = mongoose.model("users");

passport.use(
  new JWT_strategy(
    {
      jwtFromRequest: ExtractJwt.fromHeader("authorization"),
      secretOrKey: process.env.JWT_KEY
    },
    async (payload, done) => {
      const user = await users.findById(payload.id);
      if (!user) {
        res.json({
          message: "you dont have an account"
        });
        return done(null, false);
      }
      done(null, user);
    }
  )
);
