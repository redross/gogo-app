const mongoose = require("mongoose");
const bikes = mongoose.model("bikes");
const findBike = require("../utils/checkBikes");

exports.check = async (req, res, next) => {
  try {
    const bike = await findBike.check(req.body.bikeId);
    if (bike == null) return res.json({ msg: "Bike id not found" });
    else if (req.body.currentLocation === req.body.nextLocation)
      return res.json({ msg: "walking is good for health" });
    else if (
      bike.lastParkLocation === null ||
      bike.lastParkLocation === req.body.currentLocation
    ) {
      return next();
    } else {
      req.lastParkLocationError = bike.lastParkLocation;
      return next();
    }
  } catch (error) {
    return res.json({ error });
  }
};
