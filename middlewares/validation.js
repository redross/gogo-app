exports.phoneNumber = (req, res, next) => {
  const phoneRE = /^[6-9]\d{9}$/;
  const { phoneNumber } = req.body;
  if (!phoneRE.test(phoneNumber))
    return res.json({ msg: "invalid phone number" });
  else return next();
};
exports.name = (req, res, next) => {
  const nameRE = /^[A-Za-z\s]+$/;
  const { name } = req.body;
  if (!nameRE.test(name)) return res.json({ msg: "invalid name" });
  else return next();
};
exports.otp = (req, res, next) => {
  const otpRE = /^\d{4}$/;
  const { otp } = req.body;
  if (!otpRE.test(otp)) return res.json({ msg: "invalid otp" });
  else return next();
};
exports.register = (req, res, next) => {
  const emailRE = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  const { email, password, confirmPassword } = req.body;
  if (!emailRE.test(email)) return res.json({ msg: "invalid email" });
  else if (password.length < 4)
    return res.json({ msg: "password should contain atleast 4 characters" });
  else if (password !== confirmPassword)
    return res.json({ msg: "password do not match" });
  else return next();
};
