const mongoose = require("mongoose");
const transactions = mongoose.model("transactions");

exports.add = async (req, res, next) => {
  try {
    const { amount, date } = req.body;
    const { id } = req.user;
    const transaction = new transactions({ userId: id, amount, date });
    await transaction.save();
    res.json({ transaction });
  } catch (error) {
    res.json({ error });
  }
};
exports.get = async (req, res, next) => {
  try {
    const { id } = req.user;
    const transaction = await transactions.find({ userId: id }).exec();
    res.json({ transaction });
  } catch (error) {
    res.json({ error });
  }
};
