const mongoose = require("mongoose");
const bikes = mongoose.model("bikes");
const users = mongoose.model("users");
const codes = require("../utils/lockCodes");
const findBike = require("../utils/checkBikes");

exports.postlockCodesAdmin = async (req, res, next) => {
  try {
    const { bikeId, numberofCodes } = req.body;
    let bike = await findBike.check(bikeId);
    if (bike != null) await bikes.remove({ bikeId });
    const lockCodes = codes.create(numberofCodes);
    const newbike = new bikes({ bikeId, lockCodes });
    await newbike.save();
    bike = await findBike.check(bikeId);
    res.json({ bike });
  } catch (error) {
    res.json({ error });
  }
};

exports.detailsAdmin = async (req, res, next) => {
  try {
    const { bikeId } = req.body;
    const bike = await findBike.check(bikeId);
    if (bike === null) return res.json({ msg: "no record of the bike found" });
    res.json({ bike });
  } catch (error) {
    res.json({ error });
  }
};

exports.getCode = async (req, res, next) => {
  try {
    const { bikeId, currentLocation, nextLocation } = req.body;
    const { user } = req;
    const bike = await bikes.findOne({ bikeId });
    await bikes.findOneAndUpdate(
      { bikeId },
      { status: true, lastParkLocation: nextLocation, lastUserId: user.id }
    );
    const money = user.money - 2;
    await users.findByIdAndUpdate(user.id, { money }).exec();
    const password = bike.lockCodes[Math.floor(Math.random() * 10)];
    res.json({ password, error: req.lastParkLocationError });
  } catch (error) {
    return res.json({ error });
  }
};
