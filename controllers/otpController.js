const mongoose = require("mongoose");
const users = mongoose.model("users");

const findUser = require("../utils/checkUser");
const OTP = require("../utils/generateOTP");
const sendOTP = require("../utils/sendOTP");

exports.sendOTP = async (req, res) => {
  const { phoneNumber, name } = req.body;
  try {
    console.log(phoneNumber);
    const user = await findUser.check(phoneNumber);
    if (user === null) {
      const otp = OTP.create();
      const newUser = new users({ name, phoneNumber, otp });
      await newUser.save();
      await sendOTP.to(phoneNumber, name, otp);
      const sendUser = {
        name: newUser.name,
        phoneNumber: newUser.phoneNumber,
        verifiedOtp: newUser.varifiedOtp
      };
      res.json({
        msg: "otp send",
        sendUser
      });
    } else if (user.varifiedOtp === false) {
      await users.remove({ phoneNumber });
      const otp = OTP.create();
      const newUser = new users({ name, phoneNumber, otp });
      await newUser.save();
      sendOTP.to(phoneNumber, name, otp);
      const sendUser = {
        name: newUser.name,
        phoneNumber: newUser.phoneNumber,
        verifiedOtp: newUser.varifiedOtp
      };
      res.json({
        msg: "otp send",
        sendUser
      });
    } else {
      res.json({ msg: "you are already registered", user });
    }
  } catch (error) {
    res.json({ error });
  }
};

exports.checkOTP = async (req, res) => {
  try {
    const { otp, phoneNumber } = req.body;
    let user = await findUser.check(phoneNumber);
    const { varifiedOtp } = user;
    const sendOTP = user.otp;
    console.log(varifiedOtp, sendOTP);
    if (user.varifiedOtp) res.json({ msg: "user already verified", user });
    else if (!user.varifiedOtp && sendOTP === otp) {
      await users
        .findOneAndUpdate({ phoneNumber }, { varifiedOtp: true })
        .exec();
      let user = await findUser.check(phoneNumber);
      res.json({ msg: "user verrified", user });
    } else if (!user.varifiedOtp && sendOTP != otp)
      res.json({ msg: "otp doesnot match" });
  } catch (error) {
    res.json({ error });
  }
};
