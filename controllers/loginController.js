const bcrypt = require("bcrypt");
const jwtToken = require("../utils/token");
const findUser = require("../utils/checkUser");

exports.login = async (req, res, next) => {
  try {
    const { phoneNumber, password } = req.body;
    const user = await findUser.check(phoneNumber);
    if (user === null) return res.json({ msg: "invalid username or password" });
    await bcrypt.compare(password, user.password, (error, result) => {
      if (result) {
        const token = jwtToken.sign(user);
        res.json({ msg: "user verified", token });
      } else res.json({ msg: "invalid username or password" });
    });
  } catch (error) {
    res.json({ error });
  }
};
