const mongoose = require("mongoose");
const users = mongoose.model("users");
var bcrypt = require("bcrypt");

const findUser = require("../utils/checkUser");

exports.register = async (req, res, next) => {
  try {
    const { phoneNumber, email, hostel, password, confirmPassword } = req.body;
    let user = await findUser.check(phoneNumber);
    if (!user.varifiedOtp) {
      res.json({ msg: "please verify your otp first" });
      return;
    } else {
      bcrypt.hash(password, 8, async (err, hash) => {
        await users.findOneAndUpdate(
          { phoneNumber },
          { email, password: hash, hostel }
        );
        let user = await findUser.check(phoneNumber);
        res.json({ msg: "registration succesfull", user });
      });
    }
  } catch (error) {
    res.json({ error });
  }
};
