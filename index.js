process.env.NODE_ENV = process.env.NODE_ENV || "DEV";
if (process.env.NODE_ENV === "DEV") {
  require("dotenv").config();
}
const express = require("express");
const bodyParser = require("body-parser");
const morgan = require("morgan");
const cors = require("cors");
const mongoose = require("mongoose");

const app = express();
mongoose.connect(process.env.DB_URI).then(() => console.log("DB CONNECTED"));
require("./models/users");
require("./models/transactions");
require("./models/bikes");

app.use(morgan("dev"));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());

const otpAuthRoute = require("./routes/otpAuth");
const registerRoute = require("./routes/register");
const loginAuthRoute = require("./routes/loginAuth");
const transactionRoute = require("./routes/transaction");
const bikeRoute = require("./routes/bikes");

app.use("/authotp", otpAuthRoute);
app.use("/register", registerRoute);
app.use("/authlogin", loginAuthRoute);
app.use("/transaction", transactionRoute);
app.use("/bikes", bikeRoute);

const port = process.env.PORT;
app.listen(port, () => console.log(`App running on port ${port}`));
